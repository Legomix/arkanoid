﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Acceso
{
    public class MP_USUARIO
    {
        private ACCESO acceso = new ACCESO();

     
        public void Insertar(BE.Usuarios usuario)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@us", usuario.Usuario));
            parametros.Add(acceso.CrearParametro("@CON", usuario.Contraseña));
            parametros.Add(acceso.CrearParametro("@PM", usuario.Puntuacionmaxima));
            acceso.Escribir("Usuario_Crear", parametros);
           
            
                
            acceso.Cerrar();

        }

        public void Editar(BE.Usuarios usuario)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@us", usuario.Usuario));
            parameters.Add(acceso.CrearParametro("@CON", usuario.Contraseña));
            parameters.Add(acceso.CrearParametro("@PM", usuario.Puntuacionmaxima));
            acceso.Escribir("Usuario_Editar", parameters);
            acceso.Cerrar();
        }
        public void EditarPU(BE.Usuarios usuario)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@PM", usuario.Puntuacionmaxima));
            acceso.Escribir("Usuario_Editar", parameters);
            acceso.Cerrar();
        }
        public void Borrar(BE.Usuarios usuario)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@us", usuario.Usuario));
            acceso.Escribir("Usuario_Borrar", parameters);
            acceso.Cerrar();
        }

        public List<BE.Usuarios> Listar()
        {
            List<BE.Usuarios> lista = new List<BE.Usuarios>();
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Usuario_Listar");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Usuarios u = new BE.Usuarios();
                u.Usuario = registro[0].ToString();
                u.Puntuacionmaxima = int.Parse(registro[2].ToString());
                lista.Add(u);

            }
        
            return lista;
        }
    }
}
