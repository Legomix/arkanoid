﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arkanoid
{
    public partial class Form1 : Form
    {

        string user = "";
        string pass = "";


        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textUser.Text = "Allow a A to z Z";
            textUser.ForeColor = Color.Gray;
            textPass.PasswordChar = '\0';
            textPass.Text = "Allow only numbers";
            textPass.ForeColor = Color.Gray;
     
        }

        private void textUser_Enter(object sender, EventArgs e)
        {

            textUser.Text = "";
            textUser.ForeColor = Color.Black;

        }
        private void textUser_Leave(object sender, EventArgs e)
        {
            user = textUser.Text;
            if (user.Equals("Allow a A to z Z"))
            {
                textUser.Text = "Allow a A to z Z";
                textUser.ForeColor = Color.Gray;
            }
            else
            {
                if (user.Equals(""))
                {
                    textUser.Text = "Allow a A to z Z";
                    textUser.ForeColor = Color.Gray;
                }
                else
                {
                    textUser.Text = user;
                    textUser.ForeColor = Color.Black;

                }
            }
            
        }

        private void textPass_Enter(object sender, EventArgs e)
        {
            textPass.Text = "";
            textPass.ForeColor = Color.Black;
            textPass.PasswordChar= '*';
        }

        private void textPass_Leave(object sender, EventArgs e)
        {
            pass = textPass.Text;
            if (pass.Equals("Allow only numbers"))
            {
                textPass.Text = "Allow only numbers";
                textPass.ForeColor = Color.Gray;
            }
            else
            {
                if (pass.Equals(""))
                {
                    textPass.PasswordChar = '\0';
                    textPass.Text = "Allow only numbers";
                    textPass.ForeColor = Color.Gray;
                }
                else
                {
                    textPass.PasswordChar = '*';
                    textPass.Text = user;
                    textPass.ForeColor = Color.Black;

                }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 f2= new Form3();
            f2.ShowDialog();
        }
    }
}
