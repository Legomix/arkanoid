﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arkanoid
{
    public partial class Form2 : Form
    {
        Negocio.Class2 gestor = new Negocio.Class2();
        BE.Usuarios gestorbe = new BE.Usuarios();
        public Form2()
        {
            InitializeComponent();
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                BE.Usuarios u = new BE.Usuarios();
                u.Usuario = textBox1.Text;
                u.Contraseña = int.Parse(textBox2.Text);
                gestor.Grabar(u);
                Enlazar();
                MessageBox.Show("Creacion correcta");
                textBox1.Text="";
                textBox2.Text = "";
            }

            catch
            {
                MessageBox.Show("Error en la creacion");
            }
        }

        public void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestor.Listar();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                BE.Usuarios u = new BE.Usuarios();
                u.Usuario = textBox1.Text;
                u.Contraseña = int.Parse(textBox2.Text);
                gestor.Modificar(u);
                Enlazar();
                MessageBox.Show("Se modifico la contraseña");
                textBox1.Text = "";
                textBox2.Text = "";
            }

            catch
            {
                MessageBox.Show("Error");
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           if( e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                textBox1.Text= row.Cells[0].Value.ToString();
                
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BE.Usuarios u = new BE.Usuarios();
            u.Usuario = textBox1.Text;            
            gestor.Borrar(u); Enlazar();
            MessageBox.Show("Se borro con Exito");
            textBox1.Text = ""; 
            textBox2.Text = "";
            Enlazar();
            
        }
    }
}
