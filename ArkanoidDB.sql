USE [Arkanoiid]
GO
/****** Object:  Table [dbo].[Jugador]    Script Date: 7/10/2020 00:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jugador](
	[Usuario] [varchar](50) NOT NULL,
	[Contraseña] [int] NOT NULL,
	[PuntacionMaxima] [int] NOT NULL,
 CONSTRAINT [PK_Jugador] PRIMARY KEY CLUSTERED 
(
	[Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Usuario_Borrar]    Script Date: 7/10/2020 00:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[Usuario_Borrar]

@us varchar (50)
as
Begin
delete from  Jugador 
where

Usuario= @us

End
GO
/****** Object:  StoredProcedure [dbo].[Usuario_Crear]    Script Date: 7/10/2020 00:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create Proc [dbo].[Usuario_Crear]
@CON int,
@PM INT,
@us varchar (50)
as
Begin

insert Jugador values (@us,@CON,@PM)

END

GO
/****** Object:  StoredProcedure [dbo].[Usuario_Editar]    Script Date: 7/10/2020 00:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[Usuario_Editar]
@CON int,
@PM INT,
@us varchar (50)
as
Begin
update Jugador set 

Contraseña=@CON,
PuntacionMaxima=@PM
where
Usuario= @us

End

GO
/****** Object:  StoredProcedure [dbo].[Usuario_Listar]    Script Date: 7/10/2020 00:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Usuario_Listar]
as
Begin

SELECT* FROM Jugador

END

GO
